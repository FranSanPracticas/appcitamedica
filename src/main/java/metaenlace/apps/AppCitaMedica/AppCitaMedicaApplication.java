package metaenlace.apps.AppCitaMedica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppCitaMedicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppCitaMedicaApplication.class, args);
	}

}
