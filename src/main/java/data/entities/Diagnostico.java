package data.entities;

import javax.persistence.*;

@Entity

public class Diagnostico {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="id")
    private int id;
    @Column(name="valoracionEspecialista")
    private String valoracionEspecialista;
    @Column(name="enfermedad")
    private String enfermedad;
    @Column(name="cita")
    private Cita cita;


    public Diagnostico(String valoracionEspecialista, String enfermedad, Cita cita) {
        this.id = 0;
        this.valoracionEspecialista = valoracionEspecialista;
        this.enfermedad = enfermedad;
        this.cita = cita;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getValoracionEspecialista() {
        return valoracionEspecialista;
    }
    public void setValoracionEspecialista(String valoracionEspecialista) {
        this.valoracionEspecialista = valoracionEspecialista;
    }

    public String getEnfermedad() {
        return enfermedad;
    }
    public void setEnfermedad(String enfermedad) {
        this.enfermedad = enfermedad;
    }

    public Cita getCita() {
        return cita;
    }
    public void setCita(Cita cita) {
        this.cita = cita;
    }




}