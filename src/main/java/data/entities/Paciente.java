package data.entities;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name="PACIENTE")
@PrimaryKeyJoinColumn(referencedColumnName = "id")
public class Paciente extends Usuario {

    //Sin id puesto que es un Usuario. MIRAR COMO PONERLO EN JPA
    @Column(name="NSS")
    private String NSS;
    @Column(name="numTarjeta")
    private String numTarjeta;
    @Column(name="telefono")
    private String telefono;
    @Column(name="direccion")
    private String direccion;
    @Column(name="citas")
    private List<Cita> citas;
    @Column(name="medicos")
    private List<Medico> medicos;


    public Paciente(String nombre, String apellidos, String usuario, String clave, String NSS, String numTarjeta, String telefono, String direccion) {
        super(nombre, apellidos, usuario, clave);
        this.NSS = NSS;
        this.numTarjeta = numTarjeta;
        this.telefono = telefono;
        this.direccion = direccion;
        this.citas = new LinkedList<Cita>();
        this.medicos = new LinkedList<Medico>();
    }


    //Getters and setters
    public String getNSS() {
        return NSS;
    }
    public void setNSS(String NSS) {
        this.NSS = NSS;
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }
    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Cita> getCitas() {
        return new LinkedList<Cita>(citas);
    }
    public void setCitas(List<Cita> citas) {
        this.citas = citas;
    }

    public List<Medico> getMedicos() {
        return new LinkedList<Medico>(medicos);
    }
    public void setMedicos(List<Medico> medicos) {
        this.medicos = medicos;
    }




}