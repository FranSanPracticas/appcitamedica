package data.entities;

import javax.persistence.*;

//TODO: Comprobar que la herencia JPA es correcta.

@Entity
@Table(name="USUARIO")
@Inheritance(strategy = InheritanceType.JOINED)
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="id")
    private int id;
    @Column(name="nombre")
    private String nombre;
    @Column(name="apellidos")
    private String apellidos;
    @Column(name="usuario")
    private String usuario;
    @Column(name="clave")
    private String clave;

    public Usuario(String nombre, String apellidos, String usuario, String clave){
        this.id = 0;
        this.usuario = usuario;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.clave = clave;
    }


    //Getters and setters

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }

}