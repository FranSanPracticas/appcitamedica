package data.entities;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="CITA")
public class Cita {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="id")
    private int id;
    @Column(name="fechaHora")
    private Date fechaHora;
    @Column(name="motivoCita")
    private String motivoCita;
    @Column(name="paciente")
    private Paciente paciente;
    @Column(name="medico")
    private Medico medico;
    @Column(name="diagnostico")
    private Diagnostico diagnostico;

    public Cita(Date fechaHora, String motivoCita, Paciente paciente, Medico medico, Diagnostico diagnostico) {
        this.id = 0;
        this.fechaHora = fechaHora;
        this.motivoCita = motivoCita;
        this.paciente = paciente;
        this.medico = medico;
        this.diagnostico = diagnostico;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getMotivoCita() {
        return motivoCita;
    }

    public void setMotivoCita(String motivoCita) {
        this.motivoCita = motivoCita;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Diagnostico getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(Diagnostico diagnostico) {
        this.diagnostico = diagnostico;
    }

}

